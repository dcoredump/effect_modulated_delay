# effect_modulated_delay

An effect for the Teensy audio lib. It is based on the chorus effect but has a modulation input, so chorus effects can be created. As modulation source AudioSynthWaveform() object can be used. Perhaps some LoPass-filtering before sending into the modulated delay should be done.


