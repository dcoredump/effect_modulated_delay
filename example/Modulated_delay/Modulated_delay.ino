/*
   Modulated_delay.ino

   (c)2019 H. Wirtz <wirtz@parasitstudio.de>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#include <Audio.h>
#include <Wire.h>
#include <SPI.h>
#include "effect_modulated_delay.h"

// Audio configuration
AudioSynthWaveform       osc;
AudioEffectEnvelope      env;
AudioEffectModulatedDelay   moddelay;
AudioSynthWaveform       modulator;
AudioMixer4              mixer;
AudioAmplifier           vol;
AudioOutputUSB           usb1;
AudioOutputI2S           i2s1;
AudioControlSGTL5000     sgtl5000_1;
AudioConnection          patchCord0(osc, env);
AudioConnection          patchCord10(env, 0, moddelay, 0);
AudioConnection          patchCord20(env, 0, mixer, 0);
AudioConnection          patchCord30(modulator, 0, moddelay, 1);
AudioConnection          patchCord40(moddelay, 0, mixer, 1);
AudioConnection          patchCord50(mixer, vol);
AudioConnection          patchCord60(vol, 0, usb1, 0);
AudioConnection          patchCord70(vol, 0, usb1, 1);
AudioConnection          patchCord80(vol, 0, i2s1, 0);
AudioConnection          patchCord90(vol, 0, i2s1, 1);

// Helper Macros
#define TIME_MS2SAMPLES(x) floor(uint32_t(x) * AUDIO_SAMPLE_RATE / 1000)
#define SAMPLES2TIME_MS(x) float(uint32_t(x) * 1000 / AUDIO_SAMPLE_RATE)

#define SERIAL_SPEED 115200
#define AUDIO_MEM 64
#define DELAY_LENGTH_SAMPLES int32_t(TIME_MS2SAMPLES(20.0)) // 10.0 ms delay buffer. 
#define SGTL5000_LINEOUT_LEVEL 29
#define MOD_WAVEFORM WAVEFORM_TRIANGLE // WAVEFORM_SINE WAVEFORM_TRIANGLE WAVEFORM_SAWTOOTH WAVEFORM_SAWTOOTH_REVERSE
#define OSC_WAVEFORM WAVEFORM_SINE // WAVEFORM_SINE WAVEFORM_TRIANGLE WAVEFORM_SAWTOOTH WAVEFORM_SAWTOOTH_REVERSE

short delayline[DELAY_LENGTH_SAMPLES];

//*************************************************************************************************
//* SETUP FUNCTION
//*************************************************************************************************

void setup()
{
  Serial.begin(SERIAL_SPEED);
  delay(220);

  // Debug output
  Serial.println(F("Modulated_delay"));
  Serial.println(F("(c)2019 H. Wirtz <wirtz@parasitstudio.de>"));
  Serial.println(F("<setup start>"));

  // start audio card
  AudioNoInterrupts();
  AudioMemory(AUDIO_MEM);

  sgtl5000_1.enable();
  sgtl5000_1.dacVolumeRamp();
  sgtl5000_1.dacVolume(1.0);
  sgtl5000_1.unmuteHeadphone();
  sgtl5000_1.volume(0.5, 0.5); // Headphone volume
  sgtl5000_1.unmuteLineout();
  sgtl5000_1.lineOutLevel(SGTL5000_LINEOUT_LEVEL);
  sgtl5000_1.audioPostProcessorEnable();
  sgtl5000_1.eqSelect(TONE_CONTROLS);
  sgtl5000_1.autoVolumeEnable();
  sgtl5000_1.enhanceBassEnable();
  Serial.println(F("Teensy-Audio-Board enabled."));

#if defined (SHOW_DEBUG) && defined (SHOW_CPU_LOAD_MSEC)
  // Initialize processor and memory measurements
  AudioProcessorUsageMaxReset();
  AudioMemoryUsageMaxReset();
#endif

  AudioInterrupts();

  Serial.print(F("AUDIO_BLOCK_SAMPLES="));
  Serial.print(AUDIO_BLOCK_SAMPLES);
  Serial.println();

  if (!moddelay.begin(delayline, DELAY_LENGTH_SAMPLES)) {
    Serial.println(F("AudioEffectModulatedDelay - begin failed"));
    while (1);
  }

  // oscillator
  osc.begin(OSC_WAVEFORM);
  osc.frequency(440.0);
  osc.phase(0);
  osc.amplitude(1.0);
  osc.offset(0.0);

  // envelope
  env.attack(9.2);
  env.hold(2.1);
  env.decay(31.4);
  env.sustain(0.6);
  env.release(84.5);

  // modulated delay
  modulator.begin(MOD_WAVEFORM);
  modulator.frequency(1.0);
  modulator.phase(0);
  modulator.amplitude(0.5);
  modulator.offset(0.0);

  // internal mixing of original signal(0), and modulated delay(1)
  mixer.gain(0, 0.5);
  mixer.gain(1, 0.5);

  // set master volume
  vol.gain(1.0);

  // init random generator
  srand(analogRead(A0));

  Serial.println(F("<setup end>"));
}

//*************************************************************************************************
//* MAIN LOOP
//*************************************************************************************************

void loop()
{
  float frq = random(400, 1000);

  osc.frequency(frq);
  Serial.print("NoteOn  ");
  Serial.println(frq, DEC);
  env.noteOn();
  delay(3000);
  Serial.print("NoteOff ");
  Serial.println(frq, DEC);
  env.noteOff();
  delay(1000);
}
